package com.ptc.ccp.ms;

import java.util.concurrent.TimeUnit;

import com.ptc.ccp.ms.ElasticClusterManagement.Message;
import com.ptc.ccp.vertxservices.reactive.VertxReactiveServer;
import com.ptc.microservices.reactive.IReactiveFramework;
import com.ptc.microservices.reactive.ReactiveService;
import com.ptc.microservices.reactive.ServiceRequest;

import io.reactivex.Single;

public class ElasticClusterManagerService extends ReactiveService
{
	public static IReactiveFramework reactiveFramework = IReactiveFramework.getDefault ();

	private String name;

	public ElasticClusterManagerService (String name)
	{
		this.name = name;
		registerMethod ("hey", this::hey, Message.TYPE);
	}

	// Asynchronous response (RxJava style)
	public void hey (ServiceRequest<Message, Message> request)
	{
		request.respond (Single.just (new Message (name)).delay (5, TimeUnit.SECONDS));
	}

	public static void main (String[] args)
	{
		reactiveFramework.createServer (args).registerEventBusService (ElasticClusterManagement.class,
				new ElasticClusterManagerService ("Test Cluster Service")).start ();
	}
}
