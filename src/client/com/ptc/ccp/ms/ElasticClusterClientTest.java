package com.ptc.ccp.ms;

import com.ptc.ccp.vertxservices.reactive.EbusReactiveConnector;
import com.ptc.microservices.reactive.IReactiveFramework;

public class ElasticClusterClientTest
{
	public static IReactiveFramework reactiveFramework = IReactiveFramework.getDefault ();

	public static void main (String[] args)
	{
		EbusReactiveConnector.connectToServerRx (ElasticClusterManagement.class).flatMap (api -> api.heyRx ("Cluster Client"))
				.doOnError (failure -> Logger.error (failure)).doFinally ( () -> reactiveFramework.shutdown ())
				.subscribe (message -> Logger.log ("Response: " + message));
		Logger.log ("Test Client started");
	}

}

