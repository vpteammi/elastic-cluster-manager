package com.ptc.ccp.ms;

import java.lang.reflect.Type;
import java.util.function.Consumer;

import com.google.common.reflect.TypeToken;
import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.JsonSerializable;
import com.ptc.microservices.reactive.ReactiveClient;
import com.ptc.microservices.reactive.ServiceMessage;
import com.ptc.microservices.reactive.ServiceResponse;

import io.reactivex.Single;

public class ElasticClusterManagement  extends ReactiveClient<ElasticClusterManagement>
{

	public ElasticClusterManagement (IReactiveConnector connection)
	{
		super (connection);
		// TODO Auto-generated constructor stub
	}

	public static class Message implements JsonSerializable
	{
		public String text;

		public Message (String text)
		{
			super ();
			this.text = text;
		}

		@SuppressWarnings ("serial")
		public static final Type TYPE = new TypeToken<ServiceMessage<Message>> ()
		{
		}.getType ();

		@Override
		public String toString ()
		{
			return "Message [text=" + text + "]";
		}
	}

	// callback API - MUST
	public void hey (Message msg, Consumer<ServiceResponse<Message>> resultHandler)
	{
		send ("hey", msg, resultHandler, Message.TYPE);
	}
	
	public Single<Message> heyRx (Message msg)
	{
		return getSingle (msg, this::hey); // standard pattern for RxJava style
	}
	
	public Single<Message> heyRx (String msg)
	{
		return heyRx (new Message (msg)); // standard pattern for helper
	}
}
