package com.ptc.ccp.ms;

import com.sm.javax.utilx.TimeUtils;

public class Logger
{
	public static void log (String msg)
	{
		System.out.println (TimeUtils.toLogTimeFormat (TimeUtils.getNow ()) + ": " + msg);
	}
	public static void error (String msg)
	{
		System.err.println (TimeUtils.toLogTimeFormat (TimeUtils.getNow ()) + ": " + msg);
	}
	public static void error (Throwable err)
	{
		System.err.println (TimeUtils.toLogTimeFormat (TimeUtils.getNow ()) + ": Exception (" + err.getLocalizedMessage () + ")");
	}
}
